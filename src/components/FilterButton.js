import React from "react";
export let buttonValue = "All";
function FilterButton(props) {
  function ButtonClicked(val) {
    console.log("Button Clicked Value is", val);
    buttonValue = val;
  }
  return (
    <button
      type="button"
      className="btn toggle-btn"
      aria-pressed="true"
      onClick={() => {
        ButtonClicked(props.name);
      }}
    >
      <span className="visually-hidden">Show </span>
      <span>{props.name}</span>
      <span className="visually-hidden"> tasks</span>
    </button>
  );
}

export default FilterButton;
