import React from "react";

export let inputData = "NULL";

function Form(props) {
  return (
    <form>
      <h2 className="label-wrapper">
        <label htmlFor="new-todo-input" className="label__lg">
          Filter by:
        </label>
      </h2>
      <input
        type="text"
        id="new-todo-input"
        className="input input__lg"
        name="text"
        autoComplete="off"
        onChange={(e) => {
          inputData = e.target.value;
          console.log(inputData);
        }}
      />
    </form>
  );
}

export default Form;
