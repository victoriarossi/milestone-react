import React from "react";

function Nation(props){
    return (
        <div className="card">
            {/* <img src={props.addNation.flag} alt="Logo" /> */}
            <h2 className="card card-title">{props.addNation.name}</h2>
            <body>Capital: {props.addNation.capital}</body>
            <body>Languages: 
                {props.addNation.languages
                .map((language, index) => {
                    return (
                        <text key={index}>
                        {' '}{language}
                        </text>
                      );
                })}</body>
            <body>Population: {props.addNation.population}</body>
            <body>Flag: {props.addNation.flag}</body>
            <body>Currency: {props.addNation.currency}</body>
        </div>
    )
}

export default Nation;