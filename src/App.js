import React, { useState, useEffect } from "react";
import Form, { inputData } from "./components/Form";
import FilterButton from "./components/FilterButton";
import Nation from "./components/Nation";
import { buttonValue } from "./components/FilterButton";

function App() {
  const [data, setData] = useState([]);
  const localButtonValue = buttonValue;
  const localInputData = inputData;
  const getData = () => {
    fetch("./data.json", {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then(function (response) {
        // console.log(response)
        return response.json();
      })
      .then(function (myJson) {
        // console.log(myJson);
        setData(myJson);
      });
  };

  useEffect(() => {
    getData();
    // console.log(local);
    // console.log(localInputData);
  }, [localButtonValue, localInputData, data]);
  return (
    <div className="todoapp stack-large">
      <h1>Diversity n Inclusion</h1>
      <Form />
      <div className="filters btn-group stack-exception">
        <FilterButton name="All" />
        <FilterButton name="Languages" />
        <FilterButton name="Currency" />
      </div>
      <ul
        role="list"
        className="todo-list stack-large stack-exception"
        aria-labelledby="list-heading"
      >
        {localButtonValue === "All" &&
          data.map(function (nation, index) {
            return (
              <li key={index}>
                {/* {console.log(nation)} */}
                <Nation addNation={nation} />
              </li>
            );
          })}

        {/* // Functionality Related to Currency Searching */}
        {localButtonValue === "Currency" &&
          data
            .filter((value) => value.currency.toUpperCase().includes(localInputData.toUpperCase()))
            .map(function (nation, index) {
              return (
                <li key={index}>
                  <Nation addNation={nation} />
                </li>
              );
            })}
        {/* // Functionality Related to Language Searching */}
        {localButtonValue === "Languages" &&
          data
            .filter((value) => value.languages.map((language) => {
              return language.toUpperCase();
            }).includes(localInputData.toUpperCase()))
            .map(function (nation, index) {
              // console.log("Value: "+localInputData);
              return (
                <li key={index}>
                  <Nation addNation={nation} />
                </li>
              );
            })}
      </ul>
    </div>
  );
}

export default App;
